#pragma once
#include <iostream>
#include "eventSystem.hpp"
#include "player.hpp"

class Game {
  static void print_intro();
  static void show_end(const Player & player);
public:
  static void play(Player & player, const EventSystem & evSys);
};
