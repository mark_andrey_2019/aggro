#pragma once
#include <string>
#include <iostream>

class Resource {
  int money, weapon, people, territory, respect, police;
public:
  Resource();
  Resource(int im, int iw, int ippl, int it, int ir, int ipol);
  Resource(const Resource & other);
  bool is_enough() const;
  void add_resource(const Resource addition);
  void del_resource(const Resource deletion);
  std::string what_shortage() const;
  void show() const;
};
