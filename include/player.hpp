#pragma once
#include "resource.hpp"
#include <string>

class Player {
  Resource havings;
  std::string name;
public:
  std::string get_name() const;
  void add_resource(const Resource addition);
  void remove_resource(const Resource deletion);
  bool is_alive() const;
  std::string what_shortage() const;
  Player();
  Player(const Resource ires, std::string iname);
  void show_havings() const;
  Resource & get_havings();
};
