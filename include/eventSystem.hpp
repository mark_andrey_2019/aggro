#pragma once
#include "event.hpp"
#include "file.hpp"
#include "vector"
#include "ctime"

class EventSystem {
  std::vector<Event1> events1;
  std::vector<Event2> events2;
  std::vector<Event3> events3;
public:
  void initialise_events();
  const Event1 * get_rand_event() const;
};
