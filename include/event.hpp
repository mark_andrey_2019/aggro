#pragma once
#include <string>
#include <iostream>
#include "resource.hpp"

class Event1 {
protected:
  std::string description;
  std::string solution1;
  Resource cost1;
public:
  virtual void happen(Resource & havings) const;
  Event1();
  Event1(std::string id, std::string is, Resource ic);
  Event1(const Event1 & other);
};

class Event2 : public Event1 {
protected:
  std::string solution2;
  Resource cost2;
public:
  virtual void happen(Resource & havings) const;
  Event2();
  Event2(const Event1 & ev, std::string is, Resource ic);
  Event2(const Event2 & other);
};

class Event3 : public Event2 {
protected:
  std::string solution3;
  Resource cost3;
public:
  virtual void happen(Resource & havings) const;
  Event3();
  Event3(const Event2 & ev, std::string is, Resource ic);
  Event3(const Event3 & other);
};
