#pragma once
#include "event.hpp"
#include <fstream>
#include <vector>

class File {
public:
  static std::vector<Event1> get_events1();
  static std::vector<Event2> get_events2();
  static std::vector<Event3> get_events3();
};
