#include "./../include/file.hpp"

std::vector<Event1> File::get_events1() {
  std::ifstream fin("./../include/events1.txt");
  std::string description, solution1;
  int money, weapon, people, territory, respect, police;
  std::vector<Event1> events;
  while (getline(fin, description)) {
    getline(fin, solution1);
    fin >> money >> weapon >> people >> territory >> respect >> police;
    std::string strEnd;
    getline(fin, strEnd);
    Resource cost = Resource(money, weapon, people, territory, respect, police);
    events.push_back(Event1(description, solution1, cost));
  }
  fin.close();
  return events;
}
std::vector<Event2> File::get_events2() {
  std::ifstream fin("./../include/events2.txt");
  std::string description, solution1, solution2;
  int money, weapon, people, territory, respect, police;
  std::vector<Event2> events;
  while (getline(fin, description)) {
    getline(fin, solution1);
    fin >> money >> weapon >> people >> territory >> respect >> police;
    std::string strEnd;
    getline(fin, strEnd);
    Resource cost1(Resource(money, weapon, people, territory, respect, police));
    getline(fin, solution2);
    fin >> money >> weapon >> people >> territory >> respect >> police;
    getline(fin, strEnd);
    Resource cost2(Resource(money, weapon, people, territory, respect, police));
    Event1 e(description, solution1, cost1);
    events.push_back(Event2(e, solution2, cost2));
  }
  fin.close();
  return events;
}
std::vector<Event3> File::get_events3() {
  std::ifstream fin("./../include/events3.txt");
  std::string description, solution1, solution2, solution3;
  int money, weapon, people, territory, respect, police;
  std::vector<Event3> events;
  while (getline(fin, description)) {
    getline(fin, solution1);
    fin >> money >> weapon >> people >> territory >> respect >> police;
    std::string strEnd;
    getline(fin, strEnd);
    Resource cost1(Resource(money, weapon, people, territory, respect, police));
    getline(fin, solution2);
    fin >> money >> weapon >> people >> territory >> respect >> police;
    getline(fin, strEnd);
    Resource cost2(Resource(money, weapon, people, territory, respect, police));
    getline(fin, solution3);
    fin >> money >> weapon >> people >> territory >> respect >> police;
    getline(fin, strEnd);
    Resource cost3(Resource(money, weapon, people, territory, respect, police));
    Event1 e(description, solution1, cost1);
    Event2 e2(e, solution2, cost2);
    events.push_back(Event3(e2, solution3, cost3));
  }
  fin.close();
  return events;
}
