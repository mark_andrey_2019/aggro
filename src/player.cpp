#include "./../include/player.hpp"

std::string Player::get_name() const {
  return name;
}
void Player::add_resource(const Resource addition) {
  havings.add_resource(addition);
}
void Player::remove_resource(const Resource deletion) {
  havings.del_resource(deletion);
}
bool Player::is_alive() const {
  return havings.is_enough();
}
std::string Player::what_shortage() const {
  return havings.what_shortage();
}
Player::Player() {
  havings = Resource();
  name = "Jonn Doe";
}
Player::Player(const Resource ires, std::string iname) {
  havings = ires;
  name  = iname;
}
void Player::show_havings() const {
  havings.show();
}
Resource & Player::get_havings(){
  return havings;
}
