#include "./../include/resource.hpp"

Resource::Resource() {
  money = 10;
  weapon = 10;
  people = 10;
  territory = 10;
  respect = 10;
  police = 10;
}
Resource::Resource(int im, int iw, int ippl, int it, int ir, int ipol) {
  money = im;
  weapon = iw;
  people = ippl;
  territory = it;
  respect = ir;
  police = ipol;
}
Resource::Resource(const Resource & other) {
  {
    money = other.money;
    weapon = other.weapon;
    people = other.people;
    territory = other.territory;
    respect = other.respect;
    police = other.police;
  }
}
bool Resource::is_enough() const {
  if ((money <= 0) || (weapon <= 0) || (people <= 0))
    return false;
  if ((territory <= 0) || (respect <= 0) || (police <= 0))
    return false;
  return true;
}
void Resource::add_resource(const Resource addition) {
  money += addition.money;
  weapon += addition.weapon;
  people += addition.people;
  territory += addition.territory;
  respect += addition.respect;
  police += addition.police;
}
void Resource::del_resource(const Resource deletion) {
  money -= deletion.money;
  weapon -= deletion.weapon;
  people -= deletion.people;
  territory -= deletion.territory;
  respect -= deletion.respect;
  police -= deletion.police;
}
std::string Resource::what_shortage() const {
  if (money <= 0)
    return "денег";
  if (weapon <= 0)
    return "оружия";
  if (people <= 0)
    return "людей";
  if (territory <= 0)
    return "территории";
  if (respect <= 0)
    return "авторитета";
  if (police <= 0)
    return "лояльности полиции";
  return "здоровья";
}
void Resource::show() const {
  std::cout << "Деньги:             "  <<  std::to_string(money) << std::endl;
  std::cout << "Оружие:             "  <<  std::to_string(weapon) << std::endl;
  std::cout << "Люди:               "  <<  std::to_string(people) << std::endl;
  std::cout << "Территория:         "  <<  std::to_string(territory) << std::endl;
  std::cout << "Авторитет:          "  <<  std::to_string(respect) << std::endl;
  std::cout << "Лояльность полиции: "  <<  std::to_string(police) << std::endl;
}
