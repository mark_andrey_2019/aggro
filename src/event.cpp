#include "./../include/event.hpp"

void Event1::happen(Resource & havings) const {
  std::cout << description << std::endl << solution1 << std::endl;
  std::string in;
  std::cin >> in;
  if (in == "1") {
    havings.add_resource(cost1);
    return;
  }
  happen(havings);
}
Event1::Event1() {
  description = "Your people are shooting on the street.";
  solution1 = "1) Just ignore";
  cost1 = Resource(0, -5, -5, 0, 0, -5);
}
Event1::Event1(std::string id, std::string is, Resource ic) {
  description = id;
  solution1 = is;
  cost1 = ic;
}
Event1::Event1(const Event1 & other) {
  description = other.description;
  solution1 = other.solution1;
  cost1 = other.cost1;
}

void Event2::happen(Resource & havings) const {
  std::cout << Event1::description << std::endl;
  std::cout << Event1::solution1 << std::endl;
  std::cout << solution2 << std::endl;
  std::string in;
  std::cin >> in;
  if (in == "1") {
    havings.add_resource(cost1);
    return;
  }
  if (in == "2") {
    havings.add_resource(cost2);
    return;
  }
  happen(havings);
}
Event2::Event2() {
  Event1();
  solution2 = "2) Bring them weapons.";
  cost2 = Resource(0, -15, 0, 0, 0, -10);
}
Event2::Event2(const Event1 & ev, std::string is, Resource ic) : Event1(ev) {
  solution2 = is;
  cost2 = ic;
}
Event2::Event2(const Event2 & other) : Event1(static_cast<const Event1 &>(other)) {
  solution2 = other.solution2;
  cost2 = other.cost2;
}

void Event3::happen(Resource & havings) const {
  std::cout << Event1::description << std::endl;
  std::cout << Event1::solution1 << std::endl;
  std::cout << solution2 << std::endl;
  std::cout << solution3 << std::endl;
  std::string in;
  std::cin >> in;
  if (in == "1") {
    havings.add_resource(cost1);
    return;
  }
  if (in == "2") {
    havings.add_resource(cost2);
    return;
  }
  if (in == "3") {
    havings.add_resource(cost3);
    return;
  }
  happen(havings);
}
Event3::Event3() {
  Event2();
  solution3 = "3) Call 911";
  cost3 = Resource(0, 0, 0, 0, -5, +10);
}
Event3::Event3(const Event2 & ev, std::string is, Resource ic) : Event2(ev) {
  solution3 = is;
  cost3 = ic;
}
Event3::Event3(const Event3 & other) : Event2(static_cast<const Event2 &>(other)) {
  solution3 = other.solution3;
  cost3 = other.cost3;
}
