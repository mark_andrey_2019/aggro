#include "./../include/game.hpp"

void Game::print_intro() {
  std::cout << std::endl;
  std::cout << "Добро пожаловать в Aggro!" << std::endl;
  std::cout << "Aggro - симулятор главны мафиозной группировки." << std::endl;
  std::cout << "В твоем распоряжении влиятельная банда,";
  std::cout << " а твоя задача - держать ее на плаву." << std::endl;
  std::cout << "Следи за ресурсами и принимай верные решения." << std::endl;
  std::cout << std::endl;
}

void Game::show_end(const Player & player) {
  std::cout << player.get_name();
  std::cout << ", вы проиграли из-за нехватки " << player.what_shortage();
  std::cout << "." << std::endl;
}

void Game::play(Player & player, const EventSystem & evSys) {
  print_intro();
  while(player.is_alive()) {
    evSys.get_rand_event()->happen(player.get_havings());
    std::cout << std::endl;
    player.show_havings();
    std::cout << std::endl;
  }
  show_end(player);
}

int main() {
  std::string name;
  std::cout << "Как вас называть?" << std::endl;
  getline(std::cin, name);
  Player player(Resource(50,50,50,50,50,50), name);
  EventSystem eventSystem;
  eventSystem.initialise_events();
  Game::play(player, eventSystem);
  return 0;
}
