#include "./../include/eventSystem.hpp"

void EventSystem::initialise_events() {
  events1 = File::get_events1();
  events2 = File::get_events2();
  events3 = File::get_events3();
}

const Event1 * EventSystem::get_rand_event() const {
  int number = events1.size() + events2.size() + events3.size();
  const int randNum = 11;
  srand(time(0));
  int answ = rand() % number;
  if (answ < events1.size())
    return &events1[answ];
  answ -= events1.size();
  if (answ < events2.size())
    return &events2[answ];
  answ -= events2.size();
  return &events3[answ];
}
