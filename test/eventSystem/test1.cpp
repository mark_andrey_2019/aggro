#include "./../../include/eventSystem.hpp"
#include "./../../src/resource.cpp"
#include "./../../src/event.cpp"
#include "./../../src/file.cpp"
#include "./../../src/eventSystem.cpp"
using namespace std;

int main() {
  EventSystem eventSystem;
  eventSystem.initialise_events();
  Resource havings(10,10,10,10,10,10);
  eventSystem.get_rand_event()->happen(havings);
  return 0;
}
