#include "./../../include/resource.hpp"
#include "./../../src/resource.cpp"
#include <iostream>
using namespace std;

int main() {
  Resource a, b(5, 5, 5, 5, 5, 5), c(a);
  cout << "A:" << endl;
  a.show();
  cout << endl;
  cout << (a.is_enough() ? "a enough;" : "a shortage;") << endl << endl;
  cout << "B:" << endl;
  b.show();
  cout << endl;
  cout << (b.is_enough() ? "b enough;" : "b shortage;") << endl << endl;
  cout << "C:" << endl;
  c.show();
  cout << endl;
  cout << (c.is_enough() ? "c enough;" : "c shortage;") << endl << endl;
  return 0;
}
