#include "./../../include/resource.hpp"
#include "./../../src/resource.cpp"
#include <iostream>
using namespace std;

int main() {
  Resource a(2, 2, 2, -1, 2, 2);
  cout << "A:" << endl;
  a.show();
  cout << endl;
  cout << (a.is_enough() ? "A enough;" : "A shortage;") << endl << endl;
  cout << "It's shortage of " << a.what_shortage() << endl;
  return 0;
}
