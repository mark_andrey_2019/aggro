#include "./../../include/resource.hpp"
#include "./../../src/resource.cpp"
#include <iostream>
using namespace std;

int main() {
  Resource a(2, 2, 2, -1, 2, 2);
  Resource b(1, 1, 1, 2, 0, 1);
  cout << "A:" << endl;
  a.show();
  cout << endl;
  cout << (a.is_enough() ? "a enough;" : "a shortage;") << endl << endl;
  cout << "B:" << endl;
  b.show();
  cout << endl;
  cout << (b.is_enough() ? "b enough;" : "b shortage;") << endl << endl;
  a.add_resource(b);
  cout << "a.add(b)" << endl << endl;
  cout << "A:" << endl;
  a.show();
  cout << endl;
  cout << (a.is_enough() ? "a enough;" : "a shortage;") << endl << endl;
  a.del_resource(b);
  cout << "a.del(b)" << endl << endl;
  cout << "A:" << endl;
  a.show();
  cout << endl;
  cout << (a.is_enough() ? "a enough;" : "a shortage;") << endl << endl;
}
