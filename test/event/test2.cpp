#include "./../../src/event.cpp"
#include "./../../src/resource.cpp"
#include <iostream>
using namespace std;

int main() {
  Event2 a, b(Event1(), "2) BAN", Resource(0,0,0,-10,0,0)), c(b);
  Resource havings(10,10,10,10,10,10);
  havings.show();
  a.happen(havings);
  havings.show();
  b.happen(havings);
  havings.show();
  c.happen(havings);
  havings.show();
  return 0;
}
