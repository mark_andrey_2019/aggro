#include "./../../src/event.cpp"
#include "./../../src/resource.cpp"
#include <iostream>
using namespace std;

int main() {
  Event1 a, b("you dead", "1) Ok", Resource(-1000,0,0,0,0,0)), c(a);
  Resource havings;
  havings.show();
  cout << endl;
  a.happen(havings);
  havings.show();
  cout << endl;
  c.happen(havings);
  havings.show();
  cout << endl;
  b.happen(havings);
  havings.show();
  return 0;
}
