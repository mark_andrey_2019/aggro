#include "./../../src/resource.cpp"
#include "./../../src/player.cpp"
#include <iostream>
using namespace std;

int main() {
  Player v(Resource(), "Valera");
  cout << v.get_name() << ":" << endl;
  v.show_havings();
  cout << endl << "Is Valera alive: " << (v.is_alive() ? "yes" : "no") << endl;
  cout << endl << "Add 10 money." << endl << endl;
  v.add_resource(Resource(10,0,0,0,0,0));
  cout << v.get_name() << ":" << endl;
  v.show_havings();
  cout << endl << "Is Valera alive: " << (v.is_alive() ? "yes" : "no") << endl;
  cout << endl << "Remove 100 money." << endl << endl;
  v.remove_resource(Resource(100,0,0,0,0,0));
  cout << v.get_name() << ":" << endl;
  v.show_havings();
  cout << endl << "Is Valera alive: " << (v.is_alive() ? "yes" : "no") << endl;
  cout << endl << "There are " << v.what_shortage() << " shortage." << endl;
  return 0;
}
