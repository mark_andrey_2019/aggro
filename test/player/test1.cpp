#include "./../../src/resource.cpp"
#include "./../../src/player.cpp"
#include <iostream>
using namespace std;

int main() {
  Resource valerasOwn;
  Player a, b(valerasOwn, "Valera");
  cout << a.get_name() << ":" << endl;
  a.show_havings();
  cout << endl;
  cout << b.get_name() << ":" << endl;
  b.show_havings();
  return 0;
}
