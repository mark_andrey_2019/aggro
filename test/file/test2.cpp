#include "./../../include/file.hpp"
#include "./../../src/file.cpp"
#include "./../../src/event.cpp"
#include "./../../src/resource.cpp"
using namespace std;

int main() {
  // To test its necessary to change paths in file.cpp
  Resource havings(10,10,10,10,10,10);
  vector<Event2> ev2;
  ev2 = File::get_events2();
  ev2[0].happen(havings);
  havings.show();
  return 0;
}
