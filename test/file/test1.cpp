#include "./../../include/file.hpp"
#include "./../../src/file.cpp"
#include "./../../src/event.cpp"
#include "./../../src/resource.cpp"
using namespace std;

int main() {
  // To test its necessary to change paths in file.cpp
  Resource havings(10,10,10,10,10,10);
  vector<Event1> ev1;
  ev1 = File::get_events1();
  ev1[0].happen(havings);
  havings.show();
  return 0;
}
