#include "./../../include/file.hpp"
#include "./../../src/file.cpp"
#include "./../../src/event.cpp"
#include "./../../src/resource.cpp"
using namespace std;

int main() {
  // To test its necessary to change paths in file.cpp
  Resource havings(10,10,10,10,10,10);
  vector<Event3> ev3;
  ev3 = File::get_events3();
  ev3[3].happen(havings);
  havings.show();
  return 0;
}
