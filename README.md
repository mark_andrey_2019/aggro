# Aggro

Text-based strategy.

Build with CMake + Ninja is tested. Description is below.

1) Make sure you have installed CMake and Ninja (```ninja-build``` in apt).
2) Open project root in terminal
3) Run ```cmake -G Ninja -S . -B cmake-build-debug```
4) Go to created dir (```cd cmake-build-debug```)
5) Run ```ninja ```

Now you have ```Aggro``` executable in ```cmake-build-debug```.

UTF-8 encoding is used, so you can face troubles in Windows console.
In that case, run```chcp 65001``` before our executable.
